package com.eri;

import android.content.SearchRecentSuggestionsProvider;

public class MyRecentSuggestionsProvider
        extends SearchRecentSuggestionsProvider {

    public static final String AUTHORITY = "com.eri.MyRecentSuggestionsProvider";

    public static final int MODE = DATABASE_MODE_QUERIES;

    public MyRecentSuggestionsProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}
